##########################################################
'''
pong game with obstacles and transformation

authors: Brad Caponigro and Clement Rames

finished 11/25/14

'''
##########################################################


from Tkinter import *
root = Tk()
import random
from math import atan, cos, sin, pi
from PIL import Image, ImageTk

height = 600
width = height * 1.618
paddleHeight = 150
paddleWidth = paddleHeight/15

class Pong(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master.title('Pong')
        frame = Frame(master, width=width, height=height)
        self.pack()

        #create ball object
        self.ball = Ball()

        #create two paddle objects
        self.p1 = Paddle('w','s')
        self.p2 = Paddle('Up','Down')


        self.buttons = set()
        self.bind_all('<Key>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))
        self.ps = 0
        
        #create playing field
        self.screen = self.create_screen()
        self.screen.pack()
        
        #create restart and quit buttons
        restart_button = Button(self, text = "Restart", command = self.restart)
        restart_button.pack()
        quit_button = Button(self, text = "Quit", command = root.destroy)
        quit_button.pack()

        #create scoreboard
        self.v = StringVar()
        score_board = Entry(master, textvariable=self.v)
        score_board.pack()
        self.v.set("player 1: " + str(self.p1.score) + " - player 2: " + str(self.p2.score))

        #create a counter variable
        self.iteration = 0

        #create some boolean variables
        self.obstacle_exists = False
        self.transformation_exists = False
        
        #stepsize = 10 milliseconds
        self.after(10, self.update)
             
    def create_screen(self):
        #create pong screen
        self.w = Canvas(self, width=width, height=height)
        self.w.pack()

        #add both paddles
        self.w.create_rectangle(0, (height - paddleHeight)/2, paddleWidth, (height + paddleHeight)/2 , fill=str(self.p1.color), tags = "p1")
        self.w.create_rectangle(width - paddleWidth, (height - paddleHeight)/2, width, (height + paddleHeight)/2, fill=str(self.p2.color), tags = "p2")

        #add border lines
        self.w.create_line(paddleWidth, height, width - paddleWidth, height)
        self.w.create_line(0, 0, width, 0)
        self.w.create_line(paddleWidth, 0, paddleWidth, height)
        self.w.create_line(width - paddleWidth, 0, width - paddleWidth, height)
        
        return self.w
        
    def update(self):
        #move ball & paddles
        self.ball.x += self.ball.vx
        self.ball.y += self.ball.vy
        self.w.delete("ball")
        self.w.create_oval(self.ball.x - self.ball.size/2, self.ball.y - self.ball.size/2, self.ball.x + self.ball.size/2, self.ball.y + self.ball.size/2, fill="black", tags = "ball")
        self.p1.move(self.buttons)
        self.p2.move(self.buttons)
        self.w.move("p1", 0, self.p1.currentVelocity)
        self.w.move("p2", 0, self.p2.currentVelocity)
        self.collision()
        self.update_score()
        # if an obstacle exists, check to see if the ball is touching it
        if self.obstacle_exists == True:
            self.ball_pos_check(self.x_obstacle, self.y_obstacle, self.x_obstacle + self.obstacle_size, self.y_obstacle + self.obstacle_size)
        #if a transformation is present, perform the necessary transformation
        if self.transformation_exists == True:
            self.choose_transformation()
        
        self.p1.currentVelocity = 0
        self.p2.currentVelocity = 0 
        #update the board every 10 milliseconds  
        self.after(10, self.update)

    def collision(self):
        #wall collision
        if self.ball.y - self.ball.size/2 <= 0:
            self.ball.vy *= -1
        elif self.ball.y + self.ball.size/2 >= height-self.ball.size:
            self.ball.vy *= -1

        #paddle collisions
        #left paddle
        if self.ball.x - self.ball.size/2 <= paddleWidth and self.ball.y > self.p1.pos - paddleHeight and self.ball.y < self.p1.pos:
            self.ball.vx *= -1
            #self.paddle_shape()
            self.ball.N += 1
        #right paddle
        if self.ball.x + self.ball.size/2 >= width - paddleWidth and self.ball.y > self.p2.pos - paddleHeight and self.ball.y < self.p2.pos:
            self.ball.vx *= -1
            #self.paddle_shape()
            self.ball.N += 1
        
        self.ball.ball_speed()
        
    def update_score(self):
        #detect points scored and update score
        if self.ball.x + self.ball.size/2 <= paddleWidth:
            self.p2.score += 1
            self.ball.__init__()
            self.create_obstacle()
            self.create_transformation()
            
        elif self.ball.x - self.ball.size/2 >= width - paddleWidth:
            self.p1.score += 1
            self.ball.__init__()
            self.create_obstacle()
            self.create_transformation()

        self.v.set("player 1: " + str(self.p1.score) + " - player 2: " + str(self.p2.score))
        
    def restart(self):
        #Return ball to initial conditions and set both players scores to zero
        self.ball.__init__()
        self.p1.score = 0
        self.p2.score = 0
        self.transformation_exists = False
        if self.obstacle_exists == True:
            self.w.delete(self.obstacle_name)
            self.obstacle_exists = False

    def ball_pos_check(self, xc1, yc1, xc2, yc2):
        #if a creature is on the board, move it vertically
        if self.obstacle_choice == 3:
            self.w.move('creature', 0, self.creature_velocity)
            self.y_obstacle += self.creature_velocity

            #reverse the direction if the creature hits the wall
            if self.y_obstacle < 0 or self.y_obstacle + self.obstacle_size > height:
                self.creature_velocity *= -1

        #checks whether ball collides with obstacle
        if self.ball.x >= xc1 and self.ball.x <= xc2 and self.ball.y >= yc1 and self.ball.y <= yc2:
            #if the ball is touching the obstacle, perform the obstacle's reaction
            self.choose_obstacle()

    def create_obstacle(self):
        #creates and draws obstacles on the canvas if one does not exist
        if self.obstacle_exists == False:
            self.obstacle_choice = random.randrange(1,5)
            self.obstacle_size = 150
            self.obstacle_name = ''
            #determines coordinates of the obstacle
            self.x_obstacle = random.randrange(self.obstacle_size, int(width - 2*self.obstacle_size))
            self.y_obstacle = random.randrange(self.obstacle_size, height - self.obstacle_size)
        
            #create portal
            if self.obstacle_choice == 1:
                 self.obstacle_name = "portal"
                 self.x_out = abs(width - self.x_obstacle)
                 self.y_out = abs(height - self.y_obstacle)
                 self.w.create_oval(self.x_obstacle, self.y_obstacle, self.x_obstacle + self.obstacle_size, self.y_obstacle + self.obstacle_size, fill = "yellow", tags = self.obstacle_name)
                 self.w.create_text(self.x_obstacle + self.obstacle_size/2, self.y_obstacle + self.obstacle_size/2, text = "Enter Portal!", font = ("Helvetica", 16), tag = 'enter')
                 self.w.create_oval(self.x_out, self.y_out, self.x_out + self.obstacle_size, self.y_out + self.obstacle_size, fill = "red", tags = self.obstacle_name)
                 self.w.create_text(self.x_out + self.obstacle_size/2, self.y_out + self.obstacle_size/2, text = "Exit", font = ("Helvetica", 16), tag = 'exit')
            
            #create breakout wall
            elif self.obstacle_choice == 2:
                self.obstacle_name = "brick"
                self.brick = PhotoImage(height = self.obstacle_size, width = self.obstacle_size, file = 'brick.gif')
                self.w.create_image(self.x_obstacle+self.obstacle_size/2, self.y_obstacle+self.obstacle_size/2, image = self.brick, tags = self.obstacle_name)

            #create creature
            elif self.obstacle_choice == 3:
                self.obstacle_name = "creature"
                self.creature_velocity = .25
                self.monster = PhotoImage( height = self.obstacle_size, width = self.obstacle_size, file = 'cookieMonster.gif')
                self.w.create_image(self.x_obstacle+self.obstacle_size/2, self.y_obstacle+self.obstacle_size/2, anchor = CENTER, image = self.monster, tags = self.obstacle_name)

            #create cloud
            elif self.obstacle_choice == 4:
                self.obstacle_name = 'cloud'
                self.catCloud = PhotoImage(height = self.obstacle_size, width = self.obstacle_size, file = 'catCloud.gif')
                self.w.create_image(self.x_obstacle+self.obstacle_size/2, self.y_obstacle+self.obstacle_size/2, image = self.catCloud, tags = self.obstacle_name)
                
            self.obstacle_exists = True

    def choose_obstacle(self):
    #determines which obstacle physics method to call based on the value 'self.obstacle_choice', randomly determined in create_obstacle()
        if self.obstacle_choice == 1:
            self.portal()
        elif self.obstacle_choice == 2:
            self.breakout_wall()
        elif self.obstacle_choice == 3:
            self.creature()
        elif self.obstacle_choice == 4:
            self.cloud()

    def breakout_wall(self):
        # reflect the ball off of the block if the ball collides with it
        if self.ball.x - 2 * self.ball.size > self.x_obstacle and self.ball.x + 2 * self.ball.size < self.x_obstacle + self.obstacle_size:
            self.ball.vy = -self.ball.vy
        else:
            self.ball.vx = -self.ball.vx
        #after a collision, delete the block
        self.w.delete(self.obstacle_name)
        self.obstacle_exists = False
    
    def creature(self):
        # reflect the ball off of the creature if the ball collides with it
        if self.ball.x - 2 * self.ball.size > self.x_obstacle and self.ball.x + 2 * self.ball.size < self.x_obstacle + self.obstacle_size:
            self.ball.vy = -self.ball.vy
        else:
            self.ball.vx = -self.ball.vx
        #after a collision, delete the creature
        self.w.delete(self.obstacle_name)
        self.obstacle_exists = False

    def portal(self):
        #transports ball from one portal to the other
        self.ball.x = self.x_out + self.obstacle_size/2
        self.ball.y = self.y_out + self.obstacle_size/2
        #deletes portal after after ball enters them
        self.w.after(200, self.w.delete(self.obstacle_name))
        self.w.delete('enter')
        self.w.delete('exit')
        self.obstacle_exists = False

    def cloud(self):
        #reset ball position to center of the cloud
        self.ball.x = self.x_obstacle + self.obstacle_size/2
        self.ball.y = self.y_obstacle + self.obstacle_size/2
        #find magnitude of current velocity
        self.ball.v = (self.ball.vx**2 + self.ball.vy**2)**0.5
        #maintaining the magnitude of the current velocity, determine new, random components of the velocity of the ball
        self.ball.vy = random.choice([-6,-5,-4,-3,-2,-1,1,2,3,4,5,6])
        self.ball.vx = random.choice([(self.ball.v**2 - self.ball.vy**2)**0.5, -(self.ball.v**2 - self.ball.vy**2)**0.5])
        self.w.delete(self.obstacle_name)
        self.obstacle_exists = False
       

    def create_transformation(self):
        if self.transformation_exists == False:
            self.transformation_choice = random.randrange(1,4)

            #change paddle speed and turn the paddle of the player with the most points blue 
            if self.transformation_choice == 1:
                #paddle_speed() is called in choose_transformation
                self.color = "blue"
                if self.p1.score > self.p2.score:
                    self.w.delete('p1')
                    self.w.create_rectangle(0, self.p1.pos - paddleHeight, paddleWidth, self.p1.pos, fill=str(self.color), tags = "p1")
                else:
                    self.w.delete('p2')
                    self.w.create_rectangle(width - paddleWidth, self.p2.pos - paddleHeight, width, self.p2.pos, fill=str(self.color), tags = "p2")

            #change paddle shape to have a triangle on it
            elif self.transformation_choice == 2:
                self.ps = random.randint(1,2)
                if self.ps == 1:
                    self.w.create_polygon(paddleWidth, self.p1.pos, paddleWidth, self.p1.pos - paddleHeight, paddleHeight/2, self.p1.pos - paddleHeight/2, fill=str(self.p1.color), tags = "p1")
                elif self.ps == 2:
                    self.w.create_polygon(width - paddleWidth, self.p2.pos, width - paddleWidth, self.p2.pos - paddleHeight, width - paddleHeight/2, self.p2.pos - paddleHeight/2, fill=str(self.p1.color), tags = "p2")

            #create a gravity field
            elif self.transformation_choice == 3:
                    #add text to let players know gravity exists
                    self.w.create_text(width/2,height/2, text = "Gravity!!!", font = ("Helvetica", 20), tag = 'gravity_text')
                    self.x_obstacle = 0
                    self.y_obstacle = 0
                    self.obstacle_size =  max(width,height)
                    #create transparent rectangle covering the entire screen
                    self.w.create_rectangle(self.x_obstacle, self.y_obstacle, self.x_obstacle + self.obstacle_size, self.y_obstacle + self.obstacle_size, tags = 'gravity')
                    #decide if gravity is upwards or downwards
                    self.gravity = random.choice([-.4,.4])

            self.transformation_exists = True

    def choose_transformation(self):
        #choose which transformation to conduct based on the value 'self.transformation_choice', randomly determined in create_transformation()
        if self.transformation_choice == 1:
            self.paddle_speed()
        elif self.transformation_choice == 2:
            self.paddle_shape()
        elif self.transformation_choice == 3:
            self.gravity_field()
        
    def paddle_speed(self):
        #halve the speed of the paddle for the player with the higher score for 3 seconds
        if self.p1.score > self.p2.score:
            self.p1.slow_down()
            paddle_affected = 1
        else:
            self.p2.slow_down()
            paddle_affected = 2
                
        self.iteration += 1

        #after 3 seconds, return paddle back to original configuation
        if self.iteration >= 300:
            self.color = "red"
            if paddle_affected == 1:
                self.w.delete('p1')
                self.w.create_rectangle(0, self.p1.pos - paddleHeight, paddleWidth, self.p1.pos, fill=str(self.color), tags = "p1")
                self.p1.speed_back_up()
            else:
                self.w.delete('p2')
                self.w.create_rectangle(width - paddleWidth, self.p2.pos - paddleHeight, width, self.p2.pos, fill=str(self.color), tags = "p2")
                self.p2.speed_back_up()
            self.transformation_exists = False
            

    def paddle_shape(self):
        #changes the way the ball bounces on the modified paddle for 3 seconds for the player selected
        if self.ps == 1:
            if self.ball.x <= paddleWidth and self.ball.y < self.p1.pos - paddleHeight/2 and self.ball.vy > 0:
                self.ball.vy *= -1
            elif self.ball.x <= paddleWidth and self.ball.y > self.p1.pos - paddleHeight/2 and self.ball.vy < 0:
                self.ball.vy *= -1
        if self.ps == 2:
            if self.ball.x >= width - paddleWidth and self.ball.y < self.p2.pos - paddleHeight/2 and self.ball.vy > 0:
                self.ball.vy *= -1
            elif self.ball.x >= width - paddleWidth and self.ball.y > self.p2.pos - paddleHeight/2 and self.ball.vy < 0:
                self.ball.vy *= -1

        self.iteration += 1
        if self.iteration >= 300:
            #after 5 seconds, reset the shape of the paddles
            self.w.delete('p1')
            self.w.delete('p2')
            self.w.create_rectangle(0, self.p1.pos - paddleHeight, paddleWidth, self.p1.pos, fill=str(self.p1.color), tags = "p1")
            self.w.create_rectangle(width - paddleWidth, self.p2.pos - paddleHeight, width, self.p2.pos, fill=str(self.p2.color), tags = "p2")
            self.iteration = 0
            self.transformation_exists = False

    def gravity_field(self):
        #add acceleration to the ball in one direction for 2 seconds
        self.ball.vy = self.ball.vy - self.gravity
        self.iteration += 1
        if self.iteration >= 200:
            #rremove gravity widget
            self.w.delete('gravity')
            self.w.delete('gravity_text')
            self.transformation_exists = False
            self.iteration = 0
            #arbitrarily reset vertical velocity to 4
            self.ball.vy = 4
        
class Ball():
    def __init__(self):
        #number of collisions = 0 initially
        self.N = 0
        #place the ball in the center of the board
        self.x = width/2
        self.y = height/2
        #magnitude of velocity
        self.v = 8
        #randomly choose the vertical component of the velocity so that it is between -6 and 6, not including 0
        self.vy = random.choice([-6,-5,-4,-3,-2,-1,1,2,3,4,5,6])
        #choose the horizontal component so that the magnitude of the velocity is self.v
        self.vx = random.choice([(self.v**2 - self.vy**2)**0.5, -(self.v**2 - self.vy**2)**0.5])
        self.size = 10

    def ball_speed(self):
        #increase speed every five paddle collision
        if self.N == 5:
           self.vx *= 1.3
           self.vy *= 1.3
           #reset numbe rof collisions
           self.N = 0

class Paddle(object):
    def __init__(self, up, down):
        #initial position
        self.pos = (height + paddleHeight) / 2
        #paddle velocity when pressed
        self.vel = height/100
        #current paddle velocity
        self.currentVelocity = 0
        #player score
        self.score = 0
        #keys bound to the paddle
        self.up_key = up
        self.down_key = down
        #paddle color is red
        self.color = "red"

    def move(self, pressed):
        # set paddle direction according to key pressed
        if self.up_key in pressed and self.down_key not in pressed and self.pos - paddleHeight >= 0:
            self.currentVelocity = -self.vel
        # set paddle direction according to key pressed
        if self.down_key in pressed and self.up_key not in pressed and self.pos <= height:
            self.currentVelocity = self.vel
        # set paddle direction according to key pressed
        if self.down_key not in pressed and self.up_key not in pressed or self.down_key in pressed and self.up_key in pressed:
            self.currentVelocity = 0
        #move paddle
        self.pos += self.currentVelocity

    def slow_down(self):
        #halve paddle velocity
        self.vel = height/200
    
    def speed_back_up(self):
        #return the velocity back to the original value
        self.vel = height/100

pong = Pong(master = root)
pong.mainloop()