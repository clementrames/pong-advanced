#Homework 2
#Authors: Clement Rames and Brad Caponigro

import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random as rd
import scipy.ndimage as ndimage
import scipy.misc as misc

#define a class with all the functions
class Trianglehw2():
    #build the class
    def __init__(self):
        self.image = np.zeros((256, 256))
    # generate a 2d array of zeros. Create 3 vertices
    def GenerateVertices(self):
        self.A = (rd.randrange(6,256), 128)
        self.B = (rd.randrange(6,128), rd.randrange(128,250))
        self.C = (rd.randrange(128,250), rd.randrange(128,250))
        self.image[self.A] = 1
        self.image[self.B] = 1
        self.image[self.C] = 1
        self.vertices = [self.A, self.B, self.C]
        return self.vertices
#algorithm to set a pixel to 1 if it is inside the 3 vertices. fill the triangle
    def IsInside(self,x,y,tri):
        self.n = len(tri)
        self.inside =False
        p1x,p1y = tri[0]
        for i in range(self.n+1):
            p2x,p2y = tri[i % self.n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            self.inside = not self.inside
            p1x,p1y = p2x,p2y
        return self.inside
#build the triangle, and makes it blurry
    def GenerateRandomTriangles(self, sigma = 3):
        self.GenerateVertices()
        for ii in range(256):
            for jj in range(256):
                if self.IsInside(ii, jj, self.vertices):
                    self.image[ii, jj] = 1
        self.image = ndimage.gaussian_filter(self.image, sigma)        
        return self.image
#rotate the triangle 100 times. Save each image into a list.
    def SpinAndSave(self, omega, prefix = 'triangle'):
        self.image_list = []
        for t in range(100):
            self.image_list.append(ndimage.rotate(self.image, omega * t, reshape=False))
        print 'Number of images:', len(self.image_list)
        self.copied_image_list = self.image_list[:]
        
#binary corner detection


#binary corner finder
#for all images
#corners = image - image with corners removed
#binaryVertexList[index] = image
    
        
    def GetBinaryVertexImage(self):
        bi_vertex_images = []
        for i in range(100):
            img = self.image_list[i]
            im = img > .0001
            closing = ndimage.morphology.binary_closing(im)
            opening = ndimage.morphology.binary_opening(im)
            bi = ndimage.morphology.binary_dilation(closing - opening).astype(np.float)*255
            bi_vertex_images.append(bi)
        return bi_vertex_images


#harris corener detection

#for all images
#Convert image to a binary array
#find corners
#perform threshold test
#HarrisVertices[i] = corners

    def GetHarrisVertexImage(self):
        harris_vertex_images = []
        for i in range(100):
            img = self.copied_image_list[i]       
            gray = img.astype(np.uint8)
            dst = cv2.cornerHarris(gray,2,3,0.02)

#result is dilated for marking the corners, not important
            dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
            img = (img > (.00001*img.max()))*255
            harris_vertex_images.append(img)
        return harris_vertex_images

def Main():
    triangle = Trianglehw2()
    triangle.GenerateRandomTriangles()
    rate = float(raw_input('Angular Velocity in deg/s> '))
    triangle.SpinAndSave(rate, prefix = 'triangle')

    images = triangle.image_list
    bi_images = triangle.GetBinaryVertexImage()
    harris_vertex_images = triangle.GetHarrisVertexImage()

    for i in range(100):
        cv2.imshow("Triangle", images[i])
        cv2.imshow("Binary Vertices", bi_images[i])
        cv2.imshow("harris Vertices", harris_vertex_images[i])
        cv2.waitKey(50)
Main()