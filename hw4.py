# -*- coding: utf-8 -*-
"""
Created on Sat Nov 01 14:22:31 2014
@author: Clement Rames and Bradley Caponigro
"""

from Tkinter import *
root = Tk()
import random

height = 600
width = height * 1.618
paddleSize = 150

class PongGUI(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master.title('Pong')
        frame = Frame(master, width=width, height=height)
        self.pack()

        #create ball object
        self.ball = Ball()
        self.p1 = Paddle('w','s')
        self.p2 = Paddle('Up','Down')

        #create two paddle objects
        self.buttons = set()
        self.bind_all('<Key>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))
        
        #create playing field
        self.screen = self.create_screen()
        self.screen.pack()
        
        #create restart and quit buttons
        restart_button = Button(self, text = "Restart", command = self.restart)
        restart_button.pack()
        quit_button = Button(self, text = "Quit", command = root.destroy)
        quit_button.pack()

        #create scoreboard
        self.v = StringVar()
        score_board = Entry(master, textvariable=self.v)
        score_board.pack()
        self.v.set("player 1: " + str(self.p1.score) + " - player 2: " + str(self.p2.score))        
        
        #stepsize = 10
        self.after(10, self.update)
             
    def create_screen(self):
        #initial screen
        self.w = Canvas(self, width=width, height=height)
        self.w.pack()
        self.w.create_rectangle(0, (height - paddleSize)/2, paddleSize/5, (height + paddleSize)/2 , fill="blue", tags = "p1")
        self.w.create_rectangle(width - paddleSize/5, (height - paddleSize)/2, width, (height + paddleSize)/2, fill="red", tags = "p2")
        return self.w
        
    def update(self):
        #move ball& paddles
        self.ball.x += self.ball.vx
        self.ball.y += self.ball.vy
        self.w.delete("ball")
        self.w.create_oval(self.ball.x - self.ball.size/2, self.ball.y - self.ball.size/2, self.ball.x + self.ball.size/2, self.ball.y + self.ball.size/2, fill="black", tags = "ball")
        #self.w.move("ball", self.ball.vx, self.ball.vy)
        self.p1.move(self.buttons)
        self.p2.move(self.buttons)
        self.w.move("p1", 0, self.p1.currentVelocity)
        self.w.move("p2", 0, self.p2.currentVelocity)
        self.collision()
        self.update_score()
        self.p1.currentVelocity = 0
        self.p2.currentVelocity = 0       
        self.after(10, self.update)

    def collision(self):
        #wall collision
        if self.ball.y <= 0:
            self.ball.vy *= -1
        elif self.ball.y >= height-self.ball.size:
            self.ball.vy *= -1
        #paddle collision
        if self.ball.x <= paddleSize/5 or self.ball.x >= width - paddleSize/5:
            if self.ball.y > self.p1.pos - paddleSize and self.ball.y < self.p1.pos or self.ball.y > self.p2.pos - paddleSize and self.ball.y < self.p2.pos:
                self.ball.vx *= -1
                self.ball.N +=1

    def update_score(self):
        #detect points scored and update score
        if self.ball.x <= 0:
            #self.score['player_1'] += 1
            self.p1.score += 1
            self.ball.x = width / 2
            self.ball.y = height / 2
        elif self.ball.x >= width:
            #self.score['player_2'] += 1
            self.p2.score += 1
            self.ball.x = width / 2
            self.ball.y = height / 2
        #print self.score
        self.v.set("player 1: " + str(self.p2.score) + " - player 2: " + str(self.p1.score))
        #print self.v
        
    def restart(self):
        #self.ball.x = width / 2
        #self.ball.y = height / 2
        self.ball.__init__()
        self.score = {'player_1' : 0, 'player_2' : 0}
        print self.ball.x
        print self.ball.y
    
class Ball():
    def __init__(self):
        #initial position and velocity
        self.N = 0
        self.x = width/2
        self.y = height/2
        self.v = 8
        self.vy = random.randrange(-6, 6)
        self.vx = (self.v**2 - self.vy**2)**0.5
        self.size = 10

    def ball_speed(self):
        #increase speed every five paddle collision
        if self.N % 5 == 0:
           self.vx *= 1.3
           self.vy *= 1.3

class Paddle(object):
    def __init__(self, up, down):
        #initial position
        self.pos = (height + paddleSize) / 2
        #paddle velocity when pressed
        self.vel = height/100
        #current paddle velocity
        self.currentVelocity = 0
        #player score
        self.score = 0
        #keys bound to the paddle
        self.up_key = up
        self.down_key = down

    def move(self, pressed):
        # set paddle direction according to key pressed
        if self.up_key in pressed and self.down_key not in pressed and self.pos - paddleSize >= 0:
            self.currentVelocity = -self.vel
        # set paddle direction according to key pressed
        if self.down_key in pressed and self.up_key not in pressed and self.pos <= height:
            self.currentVelocity = self.vel
        # set paddle direction according to key pressed
        if self.down_key not in pressed and self.up_key not in pressed or self.down_key in pressed and self.up_key in pressed:
            self.currentVelocity = 0
        #move paddle
        self.pos += self.currentVelocity

pong = PongGUI(master = root)
pong.mainloop()
