#Homework 2

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random as rd
import scipy.ndimage as ndimage
import scipy.misc as misc

class Trianglehw2():
    
    def __init__(self):
        self.image = np.zeros((256, 256))
        
    def GenerateVertices(self):
        self.A = (rd.randrange(6,256), 128)
        self.B = (rd.randrange(6,128), rd.randrange(128,250))
        self.C = (rd.randrange(128,250), rd.randrange(128,250))
        self.image[self.A] = 1
        self.image[self.B] = 1
        self.image[self.C] = 1
        self.vertices = [self.A, self.B, self.C]
        return self.vertices

    def IsInside(self,x,y,tri):
        self.n = len(tri)
        self.inside =False
        p1x,p1y = tri[0]
        for i in range(self.n+1):
            p2x,p2y = tri[i % self.n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            self.inside = not self.inside
            p1x,p1y = p2x,p2y
        return self.inside

    def GenerateRandomTriangles(self, sigma = 3):
        self.GenerateVertices()
        for ii in range(256):
            for jj in range(256):
                if self.IsInside(ii, jj, self.vertices):
                    self.image[ii, jj] = 1
        self.image = ndimage.gaussian_filter(self.image, sigma)        
        return self.image

    def SpinAndSave(self, omega, prefix = 'triangle'):
        self.image_list = []
        for t in range(100):
            self.image_list.append(ndimage.rotate(self.image, omega * t, reshape=False))
            self.fn = '%s_%03d.png' % (prefix, t)
            misc.imsave(self.fn, self.image_list[t])
        self.im = plt.imshow(self.image_list[0], cmap='gray')
        print 'Number of images:', len(self.image_list)
        
    def Animate(self,i):
        self.im.set_array(self.image_list[i])
        return self.im,

def Main():
    fig = plt.figure()
    triangle = Trianglehw2()
    blur = int(raw_input('Blurryness of the image, 0-10: '))
    triangle.GenerateRandomTriangles(blur)
    rate = float(raw_input('Angular Velocity in deg/s: '))
    triangle.SpinAndSave(rate, prefix = 'triangle')
    ani = animation.FuncAnimation(fig,triangle.Animate,frames=100,\
          blit=False,interval=10)
    plt.show()

Main()