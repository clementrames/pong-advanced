#Data generation: random triangles
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
import random as rd
from glob import glob

def GenerateVertices():
	global image
	image = np.zeros((256,256))
	A = (rd.randrange(0,256), 128)
	B = (rd.randrange(0,256), rd.randrange(0,256))
	C = (rd.randrange(0,256), rd.randrange(0,256))
	image[A] = 1
	image[B] = 1
	image[C] = 1
	triangle = [A, B, C]
	return triangle
# determine if a point is inside a given polygon or not
# Polygon is a list of (x,y) pairs.

def IsInside(x,y,tri):

    n = len(tri)
    inside =False

    p1x,p1y = tri[0]
    for i in range(n+1):
        p2x,p2y = tri[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y
    return inside

def GenerateRandomTriangles():
	vertices = GenerateVertices()
	for ii in range(256):
		for jj in range(256):
			if IsInside(ii, jj, vertices):
				image[ii, jj] = 1
	return image

def PlotImage(sigma):
	GenerateRandomTriangles()
	blurry = ndimage.gaussian_filter(image, sigma)
	plt.figure(figsize=(10,5))
	plt.imshow(blurry)
	plt.show()

PlotImage(3)
